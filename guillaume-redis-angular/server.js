'use strict';

var express = require('express');
var app = express();
var Redis = require('redis');
var _ = require('lodash');
var path = require('path');
var bodyParser = require('body-parser');
var client = Redis.createClient(1346, 'bdzgr88ys-redis.services.clever-cloud.com', {
  auth_pass: 'Tvl5rmiEdtAy7oqh12x' || null
});


app.use(express.static(path.join(__dirname, 'public')));
//app.use(bodyParser());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

function parseLinks(zset) {
  return _(zset)
    .map(JSON.parse) // ->  [JS, 1231424543, JS, 34234234324]
    .chunk(2) // ->  [[JS, 1231424543], [JS, 34234234324]]
    .map(function(juncture) {
      var newObject = juncture[0]; // -> JS
      newObject.createdAt = juncture[1];
      return newObject;
    }).value();
}

function parseTags(zset) {
  return _(zset)
    .chunk(2)
    .map(function(juncture) {
      return {
        name: juncture[0], // redis-nl-reading
        count: _.parseInt(juncture[1]) // 303
      };
    }).value();
};
//--------------------Fonction de récupération des links en fonction des tags-----
// function search(zset, tags){
//   return _(zset)
//     .map(JSON.parse)
//     .chunk(2)
//     .map(function(juncture) {
//       var newObject = juncture[0]
//     })

// };
//--------------------------------------------------------------------------------


app.get('/links', function(req, res) {
  var limit = +req.query.limit || 10;
  var offset = +req.query.offset || 0;
  client.zrevrangebyscore(['links', '+inf', '-inf', 'WITHSCORES'], function(err, links) {
    if (err) {
      return res.send(err);
    }
    links = parseLinks(links);
    res.send({
      count: links.length,
      links: links.slice(offset, limit + offset)
    });
  });
});

//------------------A COMPLETER POUR RECHERCHER VIA CE QUI EST DANS L'INPUT----------//
// app.get('/links/search', function(req, res) {
//   client.zrevrangebyscore(['linksbytags', '+inf', '-inf', 'WITHSCORES'], function(err, linksbytags) {
//     if (err) {
//       return res.send(err);
//     }
//     res.send(parseLinks(links));
//   });
// });
//------------------------------------------------------------------------------------//

app.get('/tags', function(req, res) {
  client.zrevrangebyscore(['tags', '+inf', '-inf', 'WITHSCORES'], function(err, tags) {
    if (err) {
      return res.send(err);
    }
    res.send(parseTags(tags));
  });
});

app.get('/links/intersect', function(req, res) {
  var limit = +req.query.limit || 10;
  var offset = +req.query.offset || 0;
  if (!req.query || !req.query.tags) {
    return res.send('you need to provide at least some tags to intersect with');
  }

  var tags = req.query.tags.split(',').map(function(tag) {
    return 'tags:' + tag;
  });

  client.sinter(tags, function(err, intersectedTags) {
    res.send(err || {
      count: intersectedTags.length,
      links: intersectedTags.map(JSON.parse).slice(offset, limit + offset)
    });
  });
});

app.listen(9000, function()  {
  console.log('ready');
});
