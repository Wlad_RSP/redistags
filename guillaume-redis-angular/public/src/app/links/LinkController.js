'use strict';

angular.module('La route en Gular').controller('LinkController', ['$scope', 'LinkService',
  function($scope, LinkService) {
    var linksByPage = 0;
    $scope.selectedTags = [];
    $scope.pagination = {
      linksCount: 0,
      currentPage: 1
    };

    function setLinks(body, stay) {
      $scope.links = body.links;
      $scope.pagination.linksCount = body.count;
      linksByPage = body.links.length;
      if(!stay) {
        $scope.setPage(1);  
      }
    }

    $scope.setPage = function(page) {
      $scope.pagination.currentPage = page;
    };

    $scope.changePage = function() {
      searchAndUpdate(false);
    }

    LinkService.findLinks().then(setLinks);

    LinkService.findTags().then(function(tags) {
      $scope.tags = tags;
    });

    $scope.remove = function(tag) {
      _.remove($scope.selectedTags, tag);
      $scope.tags.push(tag);
      searchAndUpdate();
    }

    $scope.searchByTag = function(tag) {
      $scope.selectedTags.push(tag);
      $scope.tags = _.filter($scope.tags, function(tag) {
        return !_.find($scope.selectedTags, tag);
      });
      searchAndUpdate();
    };

    // $scope.searchBar = function(txt) {
    //   $scope.tags = _.filter($scope.tags, function(txt){
    //     return !_.find($scope.tags, function(txt){
    //       return 
    //     };
    //   });
    // };

    // $scope.searchBar = function(txt){
    //   _.forEach($scope.tags, function(txt){
        
    //   });
    // }


    function searchAndUpdate(stay) {
      if(!$scope.selectedTags.length){
        LinkService.findLinks({
            offset: ($scope.pagination.currentPage - 1) * linksByPage,
            limit: 10
          }).then(function(body){
            setLinks(body, true)
          }) 
        }  
      else {
        LinkService.searchByTags({
        offset: ($scope.pagination.currentPage - 1) * linksByPage,
        limit: 10,
        tags: $scope.selectedTags.map(function(tag) {
            return tag.name;
          })
        }).then(function(body){
          setLinks(body, true)
        })
      }
    }
  }
]);
