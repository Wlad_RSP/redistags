/*globals io, Visibility, _ */
'use strict';

var app = angular.module('La route en Gular', ['restangular', 'ui.bootstrap']);

app.config(function(RestangularProvider) {
  RestangularProvider.setBaseUrl('http://localhost:9000');
});
