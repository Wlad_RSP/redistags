'use strict';

angular.module('La route en Gular')
  .factory('LinkService', ['Restangular', function(Restangular) {
    function compact(obj) {
      return _.omitBy(obj, function(v) {
        return _.isArray(v) ? _.isEmpty(v) : !v;
      });
    }

    function formatLinksOptions(options) {
      options = options || {};
      options.tags = (options.tags || []).join(',');
      return compact(options);
    }

    function findLinks(options) {
      return Restangular.all('links').customGET('', options);
    }

    function findTags() {
      return Restangular.all('tags').customGET('');
    }

    // do a request on localhost:n/links/intersect?tags=tag
    function searchByTags(options) {
      return Restangular.all('links').customGET('intersect', formatLinksOptions(options));
    }

    return {
      findLinks: findLinks,
      findTags: findTags,
      searchByTags: searchByTags
    };
  }]);
